<?php

/**
 * @file
 * flip_card style plugin for the Views module.
 */

/**
 * Implements a style type plugin for the Views module.
 */
class flip_card_views_plugin_style_flip_card extends views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options += array(
      'front_fields' => array('default' => array()),
      'back_fields' => array('default' => array()),
    );
    return $options;
  }

  /**
   * Show a form to edit the style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['flip_card'] = array(
      '#type' => 'fieldset',
      '#title' => t('Flip Card'),
    );
    $field_options = $this->display->handler->get_field_labels();
    $form['flip_card']['front_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Front fields'),
      '#options' => $field_options,
      '#default_value' => $this->options['front_fields'],
    );
    $form['flip_card']['back_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Back fields'),
      '#options' => $field_options,
      '#default_value' => $this->options['back_fields'],
    );
  }

  /**
   * Performs some cleanup tasks on the options array before saving it.
   */
  function options_submit(&$form, &$form_state) {
    $options =& $form_state['values']['style_options'];
    // In some cases, when the style settings are overriden for this
    // display, the flip_card options aren't in a sub array.
    if (!empty($options['flip_card'])) {
      // Pull the fieldset values one level up.
      $options += $options['flip_card'];
      unset($options['flip_card']);
    }
  }

  /**
   * Render the display in this style.
   */
  function render() {
    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.
    $output = '';
    foreach ($sets as $title => $rows) {
      $front_fields = $this->options['front_fields'];
      $back_fields = $this->options['back_fields'];

      $front_fields_data = array();
      foreach ($rows as $index => $row) {
        $rows[$index]->front_fields = array();
        $rows[$index]->back_fields = array();
        foreach ($front_fields as $field_name => $data) {
          if (!empty($data)) {
            $rows[$index]->front_fields[$field_name] = $this->rendered_fields[$index][$field_name];
          }
        }
        foreach ($back_fields as $field_name => $data) {
          if (!empty($data)) {
            $rows[$index]->back_fields[$field_name] = $this->rendered_fields[$index][$field_name];
          }
        }
      }
      $output .= theme($this->theme_functions(),
        array(
          'view' => $this->view,
          'options' => $this->options,
          'rows' => $rows,
          'title' => $title,
        )
      );
    }

    return $output;
  }

}
