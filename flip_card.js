/**
 * @file
 * Flip Card js.
 */

(function ($) {
Drupal.behaviors.FlipCard = {
  attach: function (context, settings) {
    $(".flip-card").once('flip', function () {
      $(this).flip();
    });
  }
}
})(jQuery);
