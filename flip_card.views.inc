<?php

/**
 * @file
 * Contains core functions for the Views module support.
 */

/**
 * Implements hook_views_plugins().
 */
function flip_card_views_plugins() {
  return array(
    'style' => array(
      'flip_card' => array(
        'title' => t('Flip Card'),
        'help' => t('Display the results in a flip card style.'),
        'handler' => 'flip_card_views_plugin_style_flip_card',
        'theme' => 'flip_card_views',
        'theme file' => 'flip_card.theme.inc',
        'theme path' => drupal_get_path('module', 'flip_card') . '/theme',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => FALSE,
      ),
    ),
  );
}
