<?php

/**
 * @file
 * Theme implementations for module.
 */

/**
 * Preprocess for theme_flip_card().
 */
function template_preprocess_flip_card(&$vars) {
  // Add library and our js.
  $path = libraries_get_path('flip');
  drupal_add_js($path . '/dist/jquery.flip.js');
  drupal_add_js(drupal_get_path('module', 'flip_card') . '/flip_card.js');
}
