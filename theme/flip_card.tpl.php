<?php

/**
 * @file
 * Template for Flip Card item.
 */
?>
<div class="flip-card">
  <?php if (!empty($front_fields)): ?>
    <div class="front">
      <?php foreach ($front_fields as $key => $content): ?>
        <?php print $content; ?>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($back_fields)): ?>
    <div class="back">
      <?php foreach ($back_fields as $key => $content): ?>
        <?php print $content; ?>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
