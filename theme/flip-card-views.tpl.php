<?php

/**
 * @file
 * Stub template file to call the main Flip Card theme.
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php $front_fields = $row->front_fields; ?>
  <?php $back_fields = $row->back_fields; ?>
  <?php print theme('flip_card', array('front_fields' => $front_fields, 'back_fields' => $back_fields)); ?>
<?php endforeach; ?>
